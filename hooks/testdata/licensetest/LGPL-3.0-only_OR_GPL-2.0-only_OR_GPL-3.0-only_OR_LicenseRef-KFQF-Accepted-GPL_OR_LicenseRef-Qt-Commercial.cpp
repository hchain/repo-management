/*
    SPDX-FileCopyrightText: 2013 Test Author <nowhere@noreply.com>

    SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
*/

#include <iostream>
using namespace std;

int main() {
    cout << "Goodbye, Welt!" << std::endl;
    return 0;
}
