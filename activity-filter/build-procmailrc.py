#!/usr/bin/python
import os
import re
import sys
import yaml
import argparse
import procmailrcgenerator

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Builds a procmailrc file to filter emails based on a set of external rules')
parser.add_argument('--rules', help='Path to a directory that contains the rule files (will be accessed recursively)', required=True)
parser.add_argument('--procmailrc-file', help='Path to the procmailrc file we should write', required=True)
args = parser.parse_args()

# First make sure the folder containing our rules exists
if not os.path.exists( args.rules ):
    print("The specified rules directory does not exist!")
    sys.exit(0)

# Prepare to gather up all of our rules
collectedRules = []

# Start collecting the rules...
for currentPath, subdirectories, filesInFolder in os.walk( args.rules, topdown=False, followlinks=False ):
    # Each folder can have multiple rules in it
    # Therefore we now go over each of the files in this folder...
    for currentFile in filesInFolder:
        # Check to see if it is a YAML file first...
        if not currentFile.endswith('.yaml'):
            continue

        # Assemble the full path to the file and try to load the file
        # We catch any exceptions in case the YAML is invalid to ensure that one invalid file doesn't bring the whole system down
        fullPath = os.path.join( currentPath, currentFile )
        try:
            rulesFile = open( fullPath, 'r' )
            rules = yaml.safe_load( rulesFile )
        except Exception as e:
            print("Error loading file '%s' with error '%s'" % (fullPath, e.message))
            continue

        # Add information on which file this rule is in for later use
        for rule in rules:
            rule['file'] = currentFile

        # Now that we have the rules loaded, add them to our collection
        collectedRules.extend( rules )

# Now that we have all of our rules we can start generating the procmailrc file
# To start with we should load up our template though
# This is always located in the same directory as this script
templatePath = os.path.join( os.path.dirname( os.path.realpath(__file__) ), 'procmailrc-template' )
with open( templatePath, 'r' ) as templateFile:
    procmailrc = templateFile.read().strip() + "\n\n"

# Now we can start going over the various rules in turn
for rule in collectedRules:
    # Basic validity checks: we need an email address and we need to know what we are subscribing it to
    if 'subscribes' not in rule or 'to' not in rule:
        print("Skipping invalid rule '%s' in '%s'" % (rule['name'], rule['file']))
        continue

    # Next we make sure we have a good email address
    if re.match("^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", rule['subscribes']) is None:
        print("Skipping invalid email address in rule '%s' in '%s'" % (rule['name'], rule['file']))
        continue

    # With that out of the way we can start building the procmailrc statements we need to match email for this rule
    # First, are we filtering for commits here?
    if rule['to'] == 'commits':
        # Invoke the appropriate generator
        procmailrc += procmailrcgenerator.commits( rule ) + "\n\n"

    # Next, are we looking for merge requests?
    elif rule['to'] == 'merge_requests':
        # Then build the appropriate procmail statements
        procmailrc += procmailrcgenerator.merge_requests( rule ) + "\n\n"

    # Otherwise perhaps we are looking at tasks?
    elif rule['to'] == 'tasks':
        # Then build the appropriate procmail statements
        procmailrc += procmailrcgenerator.tasks( rule ) + "\n\n"

    # Then perhaps it is bugs we want to subscribe to?
    elif rule['to'] == 'bugs':
        # Then build the appropriate procmail statements
        procmailrc += procmailrcgenerator.bugs( rule ) + "\n\n"

# With the generation all done, we now rewrite the procmailrc file
# To avoid any issues, we try to do this atomically using a temporary file....
tempfileName = args.procmailrc_file + '.new'
with open(tempfileName, 'w') as tempfile:
    tempfile.write( procmailrc )

# Then we move the temporary file to it's final destination
os.replace( tempfileName, args.procmailrc_file )

# All done!
sys.exit(0)
